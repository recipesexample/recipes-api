const recipes = require('./recipes/recipes.service.js');
const users = require('./users/users.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(recipes);
  app.configure(users);
};
